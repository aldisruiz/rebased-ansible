# Nginx role
This role installs and sets up a Nginx configuration that makes use of the h5bp (HTML5 boilerplate) Nginx server configs.

## Attribution

[Trellis - roots.io](https://roots.io/trellis)
[h5bp - server-configs-nginx](https://github.com/h5bp/server-configs-nginx)
