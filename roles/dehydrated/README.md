# A Dehydrated (Let's Encrypt) role for Ansible

Installs and configures [dehydrated](https://github.com/dehydrated-io/dehydrated), a small Shell-based client for [Let’s encrypt](https://letsencrypt.org).

During each role run, the certificate renewal script is also executed (as with the cron job), to ensure you get new certificates as soon as you have configured them.

## Existing account keys
TODO

## Role Variables

You might want to adjust these variables that control where the software and data are located:

  * `dehydrated_install_dir`: The location to which dehydrated is cloned
  * `dehydrated_config_dir`: The location where the configuration, account key(s) and the certificate list (domains.txt) are placed
  * `dehydrated_challenges_dir`: The (web-reachable) directory that contains the temporary challenges used for 
    verifying your domain ownership downloaded.
  * `letsencrypt_account_key_source_file`: the path to the local account key file to copy over to the server. Leave this variable undefined to let this role generate the account key.
  * `letsencrypt_account_key_source_contents`: the actual content of the key file including the BEGIN and END headers. Leave this variable undefined to let this role generate the account key.

You can also adjust the user and group used for generating the certificates; there should be a dedicated user for this (recommended by the acme-tiny authors). The user and group are configured with these two variables:

  * `letsencrypt_user`
  * `letsencrypt_group`

Add the certificates to generate to their respective hosts (important! if the certificate is not generated on the host the DNS A record points to, Let’s encrypt won’t be able to verify if the hostname really belongs to you and thus won’t give you the certificate!):

    letsencrypt_certs:
      - name: "an_easily_recognizable_name__this_is_used_for_the_csr_file"
        host:
          - "myhost.example.com"
      - name: "multidomain cert"
        host:
          - "foo.example.org"
          - "bar.example.org"

For multidomain certificates, all mentioned names must point to the server where the certificate is being generated.

## Example Playbook

    - hosts: servers
      roles:
         - role: dehydrated
      
      vars:
        letsencrypt_certs:
          - name: "think_of_something_unique_or_nice_here"
            host:
              - "myhost.example.com"

## License

<!-- MIT -->

## Attribution

This role is based on [ansible-role-letsencrypt](https://github.com/andreaswolf/ansible-role-letsencrypt) by Andreas Wolf.
