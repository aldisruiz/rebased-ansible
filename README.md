# rebased-ansible
An [Ansible](https://github.com/ansible/ansible/) project for provisioning a normie-friendly [Pleroma](https://git.pleroma.social/pleroma/pleroma/) instance, on an **Ubuntu 22.04** Jammy LTS server.
It will include the following:

* Latest Pleroma OTP release, with the following:
  * [Soapbox frontend (develop branch)](https://gitlab.com/soapbox-pub/soapbox)
  * Soapbox About/ToS/Privacy/DMCA pages
  * [RUM indexes](https://docs-develop.pleroma.social/backend/configuration/cheatsheet/#rum-indexing-for-full-text-search)
  * [Suggested hardening configuration settings](https://docs-develop.pleroma.social/backend/configuration/hardening/)
  * [Optional software packages for extra functionality](https://docs-develop.pleroma.social/backend/installation/optional/media_graphics_packages/)
* Automatic SSL support via [Dehydrated](https://github.com/dehydrated-io/dehydrated) and [Let's Encrypt](https://letsencrypt.org/)(scores an A+ on the [Qualys SSL Labs Test](https://www.ssllabs.com/ssltest/))
* Nginx
* PostgreSQL
* sSMTP
* Logrotate
* Mailpit (development)
* Fail2ban and Ferm

## Documentation
I'm too lazy to write full documentation, if you need help talk to me on fedi [@aldis@sheep.network](https://sheep.network/@Aldis) or via email [admin@sheep.network](mailto:admin@sheep.network).

## Local requirements
This project has only been tested on **Linux**, other operating systems can use the remote provisioning but not the local test server.

You must have the following installed:

* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) == 2.12.3

And to make use of the local test server, you will need:

* [Virtualbox](https://www.virtualbox.org/wiki/Downloads) >= 6.1.32
* [Vagrant](https://www.vagrantup.com/downloads.html) >= 2.2.19

## Quick setup

### Installation
1. Clone rebased-ansible

    `$ git clone https://gitlab.com/aldisruiz/rebased-ansible`

2. Install Galaxy requirements

    `$ ansible-galaxy install -r galaxy.yml`

### Local test server (development)
1. Run `$ vagrant up`
2. Open `pleroma.test` in your web browser and accept a temporary exception for the self-signed SSL certificate
3. Open `pleroma.test:8025` to view Mailpit

### Remote server (staging/production)

A base **Ubuntu 22.04** (Jammy) server with at least **2GB** of RAM is required, and your domain should point to the server IP.
Additionally, a mail service is required if you want users to receive emails.

1. Add server IP/hostname to `hosts/<environment>`
2. Specify public SSH keys for `admin_user` and `web_user` in `group_vars/all/users.yml`
3. Configure server variables in:

    * `group_vars/all/main.yml` - `ntp_timezone`
    * `group_vars/all/mail.yml` - all variables
    * `group_vars/all/vault.yml` - `vault_mail_password`

4. Configure Pleroma variables in:

    * `group_vars/<environment>/main.yml` - `pleroma_settings.host`
      * View `group_vars/development/main.yml` for an example of customization
    * `group_vars/<environment>/vault.yml` - all variables

5. Provision the server:

    `$ ansible-playbook server.yml -e env=<environment>`

**NOTE**: Don't forget to encrypt `vault.yml` files before commiting them to a repository:

    $ ansible-vault encrypt group_vars/all/vault.yml group_vars/development/vault.yml group_vars/staging/vault.yml group_vars/production/vault.yml

## Known issues

1. If you have a lot `IdentityFile` entries in your `~/.ssh/config`, fail2ban may inadvertently ban (10m) you for attempting all those entries.

    You can either wait out the 10m ban, or specify which SSH key to use with an entry in `~/.ssh/config`.

## Attribution
Thank you very much to the authors and various contributors of [**Trellis**](https://github.com/roots/trellis/) (made by [Roots](https://roots.io)), rebased-ansible is based on this project.
