ANSIBLE_PATH = __dir__ # absolute path to Ansible directory on host machine

require File.join(ANSIBLE_PATH, 'lib', 'vagrant')
require 'yaml'

vconfig = YAML.load_file("#{ANSIBLE_PATH}/vagrant.default.yml")

# Load git ignored local vagrant config
if File.exist?("#{ANSIBLE_PATH}/vagrant.local.yml")
  local_config = YAML.load_file("#{ANSIBLE_PATH}/vagrant.local.yml")
  vconfig.merge!(local_config) if local_config
end

Vagrant.configure('2') do |config|
  config.vagrant.plugins = ['vagrant-hostmanager']
  config.vm.box = vconfig.fetch('vagrant_box')
  config.vm.box_version = vconfig.fetch('vagrant_box_version')
  config.vm.hostname = vconfig.fetch('vagrant_hostname') # required by vagrant-hostmanager
  config.vm.network :private_network, ip: vconfig.fetch('vagrant_ip'), hostsupdater: 'skip'
  config.vm.post_up_message = "Your Pleroma Vagrant box is ready to use!
  * Any pleroma_ctl command will need to be run on the virtual machine.
  * You can SSH into the machine with `vagrant ssh`.
  * Then navigate to `/srv/www/pleroma`"

  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.manage_guest = false

  config.vm.provision "ansible" do |ansible|
    ansible.verbose = vconfig.fetch('vagrant_ansible_verbose')
    ansible.compatibility_mode = '2.0'
    ansible.playbook = File.join(ANSIBLE_PATH, 'dev.yml')
    ansible.galaxy_role_file = File.join(ANSIBLE_PATH, 'galaxy.yml') unless vconfig.fetch('vagrant_skip_galaxy') || ENV['SKIP_GALAXY']
    ansible.galaxy_roles_path = File.join(ANSIBLE_PATH, 'vendor/roles')

    ansible.groups = {
      'web' => ['default'],
      'development' => ['default']
    }

    ansible.tags = ENV['ANSIBLE_TAGS']

    config.trigger.after :up do |trigger|
      # Add Vagrant ssh-config to ~/.ssh/config
      trigger.info = "Adding vagrant ssh-config for #{config.vm.hostname} to ~/.ssh/config"
      trigger.ruby do
        update_ssh_config(config.vm.hostname)
      end
    end
  end

  # VirtualBox settings
  config.vm.provider 'virtualbox' do |vb|
    vb.name = config.vm.hostname
    vb.customize ['modifyvm', :id, '--cpus', vconfig.fetch('vagrant_cpus')]
    vb.customize ['modifyvm', :id, '--memory', vconfig.fetch('vagrant_memory')]
  end
end
