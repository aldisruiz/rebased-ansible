import os

from ansible.module_utils.six import iteritems
from ansible.playbook.play_context import PlayContext
from ansible.plugins.callback import CallbackBase
from ansible import context


class CallbackModule(CallbackBase):
    ''' Creates and modifies play and host variables '''

    CALLBACK_VERSION = 2.0
    CALLBACK_NAME = 'vars'

    def __init__(self):
        super(CallbackModule, self).__init__()
        self._options = context.CLIARGS

    def cli_options(self):
        options = []

        strings = {
            '--connection': 'connection',
            '--private-key': 'private_key_file',
            '--ssh-common-args': 'ssh_common_args',
            '--ssh-extra-args': 'ssh_extra_args',
            '--timeout': 'timeout',
            '--vault-password-file': 'vault_password_file',
            }

        for option,value in iteritems(strings):
            if self._options.get(value, False):
                options.append("{0}='{1}'".format(option, str(self._options.get(value))))

        for inventory in self._options.get('inventory'):
            options.append("--inventory='{}'".format(str(inventory)))

        if self._options.get('ask_vault_pass', False):
            options.append('--ask-vault-pass')

        return ' '.join(options)

    def v2_playbook_on_play_start(self, play):
        play_context = PlayContext(play=play)

        env = play.get_variable_manager().get_vars(play=play).get('env', '')
        env_group = next((group for key,group in iteritems(play.get_variable_manager()._inventory.groups) if key == env), False)
        if env_group:
            env_group.set_priority(20)

        for host in play.get_variable_manager()._inventory.list_hosts(play.hosts[0]):
            hostvars = play.get_variable_manager().get_vars(play=play, host=host)
            host.vars['cli_options'] = self.cli_options()
            host.vars['cli_ask_pass'] = self._options.get('ask_pass', False)
            host.vars['cli_ask_become_pass'] = self._options.get('become_ask_pass', False)
