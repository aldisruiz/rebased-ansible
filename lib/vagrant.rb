def update_ssh_config(main_hostname)
  regexp = /(Host #{Regexp.quote(main_hostname)}(?:(?!^Host).)*)/m
  config_file = File.expand_path('~/.ssh/config')
  vagrant_ssh_config = `vagrant ssh-config --host #{main_hostname}`.chomp

  if File.exists?(config_file)
    FileUtils.cp(config_file, "#{config_file}.rebased-ansible_backup")
    ssh_config = File.read(config_file)

    content = if ssh_config =~ regexp
      ssh_config.gsub(regexp, vagrant_ssh_config)
    else
      ssh_config << "\n#{vagrant_ssh_config}"
    end

    File.write(config_file, content)
  else
    FileUtils.mkdir_p(File.dirname(config_file), mode: 0700)
    File.write(config_file, vagrant_ssh_config)
  end
end
